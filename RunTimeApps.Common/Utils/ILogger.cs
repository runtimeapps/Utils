﻿using System;
using System.Threading.Tasks;

namespace RunTimeApps.Common.Utils
{
    public interface ILogger : IDisposable
    {
        void Log(string message, int subSystem, int level);
        Task LogAsync(string message, int subSystem, int level);
        void Log(Exception exception, int subSystem, int level);
        Task LogAsync(Exception exception, int subSystem, int level);
    }
}