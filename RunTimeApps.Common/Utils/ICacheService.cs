﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RunTimeApps.Common.Utils
{
    public interface ICacheService : IDisposable
    {
        T GetValue<T>(string key, string region = null);
        Task<T> GetValueAsync<T>(string key, string region = null);
        IDictionary<string, T> GetValues<T>(string[] keys);
        Task<IDictionary<string, T>> GetValuesAsync<T>(string[] keys);
        void SetValue<T>(string key, T value, TimeSpan? lifeTime);
        Task SetValueAsync<T>(string key, T value, TimeSpan? lifeTime);
        void SetValue<T>(string key, T value, DateTimeOffset? expireTime);
        Task SetValueAsync<T>(string key, T value, DateTimeOffset? expireTime);
        void SetValue<T>(string key, T value);
        Task SetValueAsync<T>(string key, T value);
        void SetString(string key, string value, TimeSpan? lifeTime);
        Task SetStringAsync(string key, string value, TimeSpan? lifeTime);
        void SetString(string key, string value);
        Task SetStringAsync(string key, string value);
        void SetValues<T>(KeyValuePair<string, T>[] pairs, TimeSpan? lifeTime);
        Task SetValuesAsync<T>(KeyValuePair<string, T>[] pairs, TimeSpan? lifeTime);
        void SetValues<T>(KeyValuePair<string, T>[] pairs);
        Task SetValuesAsync<T>(KeyValuePair<string, T>[] pairs);
        void SetStrings(KeyValuePair<string, string>[] pairs, TimeSpan? lifeTime);
        Task SetStringsAsync(KeyValuePair<string, string>[] pairs, TimeSpan? lifeTime);
        void SetStrings(KeyValuePair<string, string>[] pairs);
        Task SetStringsAsync(KeyValuePair<string, string>[] pairs);
        string GetString(string key, string region = null);
        Task<string> GetStringAsync(string key, string region = null);
        string[] GetStrings(string[] keys, string region = null);
        Task<string[]> GetStringsAsync(string[] keys, string region = null);
        void Delete(string key);
        Task DeleteAsync(string key);
    }
}