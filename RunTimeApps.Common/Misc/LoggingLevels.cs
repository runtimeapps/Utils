﻿namespace RunTimeApps.Common
{
    public enum LoggingLevels
    {
        FatalError = 1,
        Warning = 2,
        Info = 3
    }
}