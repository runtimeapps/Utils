﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using System.Threading;
using RunTimeApps.Common.Utils;
using RunTimeApps.Caching.LocalCache.Models;

namespace RunTimeApps.Caching.LocalCache
{
    public class LocalCache : ICacheService
    {
        private readonly ConcurrentDictionary<string, CacheItem> _pool = null;
        public LocalCache()
        {
            _pool = new ConcurrentDictionary<string, CacheItem>();
        }

        public void Delete(string key)
        {
            _pool.TryRemove(key, out CacheItem value);
        }

        public async Task DeleteAsync(string key)
        {
            await Task.Run(() => Delete(key));
        }

        public string GetString(string key, string region = null)
        {
            return _pool.TryGetValue(key, out CacheItem value) ? !value.Expired ? value.Data.ToString() : RemoveExipred<string>(key) : null;
        }

        public async Task<string> GetStringAsync(string key, string region = null)
        {
            return await Task.FromResult(GetString(key, region));
        }

        public string[] GetStrings(string[] keys, string region = null)
        {
            int length = keys.Length;
            string[] results = new string[length];
            for (int i = 0; i < length; i++)
                results[i] = _pool.TryGetValue(keys[i], out CacheItem value) ? !value.Expired ?
                             value.Data.ToString() : RemoveExipred<string>(keys[i]) : null;
            return results;
        }

        public async Task<string[]> GetStringsAsync(string[] keys, string region = null)
        {
            return await Task.FromResult(GetStrings(keys, region));
        }

        public T GetValue<T>(string key, string region = null)
        {
            return _pool.TryGetValue(key, out CacheItem value) ? !value.Expired ? (T)value.Data : RemoveExipred<T>(key) : default(T);
        }

        public async Task<T> GetValueAsync<T>(string key, string region = null)
        {
            return await Task.FromResult(GetValue<T>(key, region));
        }

        public IDictionary<string, T> GetValues<T>(string[] keys)
        {
            int length = keys.Length;
            IDictionary<string, T> result = new Dictionary<string, T>(length);
            for (int i = 0; i < length; i++)
                result.Add(keys[i], _pool.TryGetValue(keys[i], out CacheItem value) ? !value.Expired ?
                                   (T)value.Data : RemoveExipred<T>(keys[i]) : default(T));
            return result;
        }

        public async Task<IDictionary<string, T>> GetValuesAsync<T>(string[] keys)
        {
            return await Task.FromResult(GetValues<T>(keys));
        }

        public void SetString(string key, string value, TimeSpan? lifeTime)
        {
            SetValue(key, value, lifeTime);
        }
        public void SetString(string key,string value)
        {
            SetString(key, value, null);
        }

        public async Task SetStringAsync(string key, string value, TimeSpan? lifeTime)
        {
            await SetValueAsync(key, value, lifeTime);
        }

        public async Task SetStringAsync(string key,string value)
        {
            await SetStringAsync(key, value, null);
        }

        public void SetValue<T>(string key, T value, TimeSpan? lifeTime)
        {
            Delete(key);
            _pool.TryAdd(key, new CacheItem(value, lifeTime));
        }

        public void SetValue<T>(string key,T value)
        {
            SetValue<T>(key, value, lifeTime: null);
        }

        public async Task SetValueAsync<T>(string key, T value, TimeSpan? lifeTime)
        {
            await Task.Run(() => SetValue(key, value, lifeTime));
        }

        public async Task SetValueAsync<T>(string key,T value)
        {
            await SetValueAsync<T>(key, value, lifeTime: null);
        }

        public void SetValue<T>(string key, T value, DateTimeOffset? expireTime)
        {
            Delete(key);
            _pool.TryAdd(key, new CacheItem(value, expireTime));
        }

        public async Task SetValueAsync<T>(string key, T value, DateTimeOffset? expireTime)
        {
            await Task.Run(() => SetValue(key, value, expireTime));
        }

        public void SetValues<T>(KeyValuePair<string, T>[] pairs, TimeSpan? lifeTime)
        {
            foreach (var item in pairs)
            {
                Delete(item.Key);
                SetValue(item.Key, item.Value, lifeTime);
            }
        }

        public void SetValues<T>(KeyValuePair<string, T>[] pairs)
        {
            SetValues<T>(pairs, null);
        }

        public async Task SetValuesAsync<T>(KeyValuePair<string, T>[] pairs, TimeSpan? lifeTime)
        {
            foreach (var item in pairs)
            {
                await DeleteAsync(item.Key);
                await SetValueAsync(item.Key, item.Value, lifeTime);
            }
        }

        public async Task SetValuesAsync<T>(KeyValuePair<string, T>[] pairs)
        {
            await SetValuesAsync<T>(pairs, null);
        }

        public void SetStrings(KeyValuePair<string, string>[] pairs, TimeSpan? lifeTime)
        {
            foreach (var item in pairs)
            {
                Delete(item.Key);
                SetString(item.Key, item.Value, lifeTime);
            }
        }

        public void SetStrings(KeyValuePair<string, string>[] pairs)
        {
            SetStrings(pairs, null);
        }

        public async Task SetStringsAsync(KeyValuePair<string, string>[] pairs, TimeSpan? lifeTime)
        {
            foreach (var item in pairs)
            {
                await DeleteAsync(item.Key);
                await SetStringAsync(item.Key, item.Value, lifeTime);
            }
        }

        public async Task SetStringsAsync(KeyValuePair<string, string>[] pairs)
        {
            await SetStringsAsync(pairs, null);
        }

        #region Private Methods
        private T RemoveExipred<T>(string key)
        {
            new Thread(() => _pool.TryRemove(key, out CacheItem value)).Start();
            return default(T);
        }
        #endregion

        #region Disposable Members
        public void Dispose()
        {
            Dispose(true);
        }

        private bool _disposed = false;
        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                _pool.Clear();
                _disposed = true;
            }
            //Dispose UnManaged Resources
        }
        ~LocalCache()
        {
            Dispose(false);
        }
        #endregion
    }
}
