﻿using System;

namespace RunTimeApps.Caching.LocalCache.Models
{
    internal class CacheItem
    {
        internal CacheItem(object data, TimeSpan? lifeTime = null)
        {
            Data = data;
            ExpireDate = lifeTime.HasValue ? DateTime.Now.Add(lifeTime.Value) : ExpireDate;
        }

        internal CacheItem(object data, DateTimeOffset? lifeTime = null)
        {
            Data = data;
            ExpireDate = lifeTime?.Date;
        }

        public DateTime? ExpireDate { get; set; }
        public object Data { get; set; }
        public bool Expired => DateTime.Now > ExpireDate;
    }
}