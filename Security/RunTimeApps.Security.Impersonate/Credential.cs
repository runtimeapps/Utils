﻿using System;
using System.Security.Principal;
using System.Runtime.InteropServices;

namespace RunTimeApps.Security.Impersonate
{
    public class Credential : IDisposable
    {
        private readonly IntPtr _token;
        private readonly WindowsImpersonationContext _windowsImpersonationContext = null;

        [DllImport("kernel32.dll")]
        private static extern Boolean CloseHandle(IntPtr hObject);

        internal Credential(IntPtr token, WindowsImpersonationContext windowsImpersonationContext)
        {
            _token = token;
            _windowsImpersonationContext = windowsImpersonationContext;
        }

        public void Logout()
        {
            _windowsImpersonationContext.Undo();
            CloseHandle(_token);
            _windowsImpersonationContext.Dispose();
        }

        #region IDisposable Memberes
        public void Dispose()
        {
            Dispose(true);
        }

        private bool _disposed = false;
        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
                Logout();
        }

        ~Credential()
        {
            Dispose(false);
        }
        #endregion
    }
}
