﻿using System;
using System.Runtime.InteropServices;
using principal = System.Security.Principal;

namespace RunTimeApps.Security.Impersonate
{
    public class WindowsIdentity
    {
        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool LogonUser(string lpszUsername, string lpszDomain, string lpszPassword,
                                             int dwLogonType, int dwLogonProvider, ref IntPtr phToken);

        public Credential Impersonate(string domain, string userName, string passWord)
        {
            IntPtr token = IntPtr.Zero;
            bool successed = LogonUser(userName, domain, passWord, 9, 0, ref token);
            if (successed)
                return new Credential(token, new principal.WindowsIdentity(token).Impersonate());
            return null;
        }
    }
}