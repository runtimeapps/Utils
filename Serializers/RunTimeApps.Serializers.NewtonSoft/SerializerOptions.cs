﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;

namespace RunTimeApps.Serializers.NewtonSoft
{
    public static class SerializerOptions
    {
        public static readonly Func<JsonSerializerSettings> Global = () => new JsonSerializerSettings()
        {
            NullValueHandling = NullValueHandling.Ignore,
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            DateFormatHandling = DateFormatHandling.IsoDateFormat,
            DateTimeZoneHandling = DateTimeZoneHandling.Unspecified
        };
    }
}